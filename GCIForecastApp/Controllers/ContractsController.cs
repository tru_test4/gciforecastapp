﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;

namespace GCIForecastApp.Controllers
{
    public class ContractsController : Controller
    {
        //
        // GET: /Contracts/
        public ActionResult Index()
        {
            return View();
        }

        GCIForecastApp.Models.GciForecastContext db = new GCIForecastApp.Models.GciForecastContext();

        [ValidateInput(false)]
        public ActionResult GridViewContractsPartial()
        {
            var model = db.Contracts;
            return PartialView("_GridViewContractsPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewContractsPartialAddNew(GCIForecastApp.Models.Contract item)
        {
            var model = db.Contracts;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewContractsPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewContractsPartialUpdate(GCIForecastApp.Models.Contract item)
        {
            var model = db.Contracts;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.ContractId == item.ContractId);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewContractsPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewContractsPartialDelete(System.Int32 ContractId)
        {
            var model = db.Contracts;
            if (ContractId >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.ContractId == ContractId);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridViewContractsPartial", model.ToList());
        }
	}
}