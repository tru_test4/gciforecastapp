﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;

namespace GCIForecastApp.Controllers
{
    public class OrganizationsController : Controller
    {
        //
        // GET: /Organizations/
        public ActionResult Index()
        {
            return View();
        }

        GCIForecastApp.Models.GciForecastContext db = new GCIForecastApp.Models.GciForecastContext();

        [ValidateInput(false)]
        public ActionResult GridViewOrganizationsPartial()
        {
            var model = db.Organizations;
            return PartialView("_GridViewOrganizationsPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewOrganizationsPartialAddNew(GCIForecastApp.Models.Organization item)
        {
            var model = db.Organizations;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewOrganizationsPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewOrganizationsPartialUpdate(GCIForecastApp.Models.Organization item)
        {
            var model = db.Organizations;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.OrganizationId == item.OrganizationId);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewOrganizationsPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewOrganizationsPartialDelete(System.Int32 OrganizationId)
        {
            var model = db.Organizations;
            if (OrganizationId >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.OrganizationId == OrganizationId);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridViewOrganizationsPartial", model.ToList());
        }
	}
}