﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;

namespace GCIForecastApp.Controllers
{
    public class TransportTypesController : Controller
    {
        // GET: TransportTypes
        public ActionResult Index()
        {
            return View();
        }

        GCIForecastApp.Models.GciForecastContext db = new GCIForecastApp.Models.GciForecastContext();

        [ValidateInput(false)]
        public ActionResult GridViewTransportTypePartial()
        {
            var model = db.TransportTypes;
            return PartialView("_GridViewTransportTypePartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewTransportTypePartialAddNew(GCIForecastApp.Models.TransportType item)
        {
            var model = db.TransportTypes;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewTransportTypePartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewTransportTypePartialUpdate(GCIForecastApp.Models.TransportType item)
        {
            var model = db.TransportTypes;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.TransportTypeId == item.TransportTypeId);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewTransportTypePartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewTransportTypePartialDelete(System.Int32 TransportTypeId)
        {
            var model = db.TransportTypes;
            if (TransportTypeId >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.TransportTypeId == TransportTypeId);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridViewTransportTypePartial", model.ToList());
        }
    }
}