﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevExpress.Web.Mvc;

namespace GCIForecastApp.Controllers
{
    public class CircuitsController : Controller
    {
        // GET: Circuit
        public ActionResult Index()
        {
            return View();
        }

        GCIForecastApp.Models.GciForecastContext db = new GCIForecastApp.Models.GciForecastContext();

        [ValidateInput(false)]
        public ActionResult GridViewCircuitPartial()
        {
            var model = db.Circuits;
            return PartialView("_GridViewCircuitPartial", model.ToList());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewCircuitPartialAddNew(GCIForecastApp.Models.Circuit item)
        {
            var model = db.Circuits;
            if (ModelState.IsValid)
            {
                try
                {
                    model.Add(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewCircuitPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewCircuitPartialUpdate(GCIForecastApp.Models.Circuit item)
        {
            var model = db.Circuits;
            if (ModelState.IsValid)
            {
                try
                {
                    var modelItem = model.FirstOrDefault(it => it.CircuitId == item.CircuitId);
                    if (modelItem != null)
                    {
                        this.UpdateModel(modelItem);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            else
                ViewData["EditError"] = "Please, correct all errors.";
            return PartialView("_GridViewCircuitPartial", model.ToList());
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewCircuitPartialDelete(System.Int32 CircuitId)
        {
            var model = db.Circuits;
            if (CircuitId >= 0)
            {
                try
                {
                    var item = model.FirstOrDefault(it => it.CircuitId == CircuitId);
                    if (item != null)
                        model.Remove(item);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    ViewData["EditError"] = e.Message;
                }
            }
            return PartialView("_GridViewCircuitPartial", model.ToList());
        }
    }
}