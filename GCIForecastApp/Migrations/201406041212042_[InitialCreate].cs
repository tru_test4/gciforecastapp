namespace GCIForecastApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Circuits",
                c => new
                    {
                        CircuitId = c.Int(nullable: false, identity: true),
                        From = c.String(nullable: false),
                        To = c.String(nullable: false),
                        BwIn = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BwOut = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransportTypeId = c.Int(nullable: false),
                        ContractId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CircuitId)
                .ForeignKey("dbo.Contracts", t => t.ContractId)
                .ForeignKey("dbo.TransportTypes", t => t.TransportTypeId)
                .Index(t => t.TransportTypeId)
                .Index(t => t.ContractId);
            
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        ContractId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 250),
                        SignatureDate = c.DateTime(nullable: false),
                        ExpirationDate = c.DateTime(nullable: false),
                        OrganizationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ContractId)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId)
                .Index(t => t.OrganizationId);
            
            CreateTable(
                "dbo.Organizations",
                c => new
                    {
                        OrganizationId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        Site = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.OrganizationId);
            
            CreateTable(
                "dbo.TransportTypes",
                c => new
                    {
                        TransportTypeId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.TransportTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Circuits", "TransportTypeId", "dbo.TransportTypes");
            DropForeignKey("dbo.Contracts", "OrganizationId", "dbo.Organizations");
            DropForeignKey("dbo.Circuits", "ContractId", "dbo.Contracts");
            DropIndex("dbo.Contracts", new[] { "OrganizationId" });
            DropIndex("dbo.Circuits", new[] { "ContractId" });
            DropIndex("dbo.Circuits", new[] { "TransportTypeId" });
            DropTable("dbo.TransportTypes");
            DropTable("dbo.Organizations");
            DropTable("dbo.Contracts");
            DropTable("dbo.Circuits");
        }
    }
}
