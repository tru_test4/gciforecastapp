using GCIForecastApp.Models;

namespace GCIForecastApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<GCIForecastApp.Models.GciForecastContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GCIForecastApp.Models.GciForecastContext context)
        {
            //  This method will be called after migrating to the latest version.

            var transportNames = new[]
            {
                "TERRA", "KU iDirect", "C-Band iDirect", "AUNE Fiber", "AUE&W Fiber", "SEAFAST", "C-Band CNC/PL", "KU CNC/PL", "Other"
            };

            var transportArray = (from str in transportNames select new TransportType { Name = str }).ToArray();

            context.TransportTypes.AddOrUpdate(t => t.Name, transportArray);

            context.SaveChanges();

            context.Organizations.AddOrUpdate(o => new { o.Name, o.Description },
            new Organization() { Name = "Test Organization Name", Description = "Test Organization Description" });

            context.Organizations.AddOrUpdate(o => new { o.Name, o.Description },
                new Organization() { Name = "New Organization", Description = "New Organization Description" });

            context.Organizations.AddOrUpdate(o => new { o.Name, o.Description },
                new Organization() { Name = "One more Organization", Description = "Description" });

            context.SaveChanges();

            var org = context.Organizations.First();

            context.Contracts.AddOrUpdate(c => new { c.SignatureDate, c.ExpirationDate },
                new Contract()
                {
                    Organization = org,
                    OrganizationId = org.OrganizationId,
                    Title = "Contract 1",
                    SignatureDate = new DateTime(2014, 1, 1),
                    ExpirationDate = new DateTime(2015, 1, 1)
                });

            context.Contracts.AddOrUpdate(c => c.Title,
                new Contract()
                {
                    Organization = org,
                    OrganizationId = org.OrganizationId,
                    Title = "Contract 2",
                    SignatureDate = new DateTime(2014, 1, 10),
                    ExpirationDate = new DateTime(2015, 1, 10)

                });

            context.Contracts.AddOrUpdate(c => c.Title,
                new Contract()
                {
                    Organization = org,
                    OrganizationId = org.OrganizationId,
                    Title = "Contract 3",
                    SignatureDate = new DateTime(2014, 1, 20),
                    ExpirationDate = new DateTime(2015, 1, 20)
                });

            context.SaveChanges();

            var contract = context.Contracts.First();


            var transportType = context.TransportTypes.First();

            context.Circuits.AddOrUpdate(c => new { c.From, c.To },
                new Circuit()
                {
                    From = "Circuit 1 From",
                    To = "Circuit 1 To",
                    BwIn = 10.5m,
                    BwOut = 5.5m,
                    Contract = contract,
                    ContractId = contract.ContractId,
                    TransportType = transportType,
                    TransportTypeId = transportType.TransportTypeId
                });

            context.SaveChanges();

            var circuit = context.Circuits.First();

            context.Forecasts.AddOrUpdate(c => c.StartDate, new Forecast()
            {
                StartDate = circuit.Contract.SignatureDate.AddYears(1),
                BwIn = circuit.BwIn * 1.5m,
                BwOut = circuit.BwOut * 1.5m,
                CircuitId = circuit.CircuitId,
                TransportTypeId = circuit.TransportTypeId
            });

            context.SaveChanges();

        }
    }
}
