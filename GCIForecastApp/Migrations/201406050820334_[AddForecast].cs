namespace GCIForecastApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForecast : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Forecasts",
                c => new
                    {
                        ForecastId = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        BwIn = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BwOut = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransportTypeId = c.Int(nullable: false),
                        CircuitId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ForecastId)
                .ForeignKey("dbo.Circuits", t => t.CircuitId)
                .ForeignKey("dbo.TransportTypes", t => t.TransportTypeId)
                .Index(t => t.TransportTypeId)
                .Index(t => t.CircuitId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Forecasts", "TransportTypeId", "dbo.TransportTypes");
            DropForeignKey("dbo.Forecasts", "CircuitId", "dbo.Circuits");
            DropIndex("dbo.Forecasts", new[] { "CircuitId" });
            DropIndex("dbo.Forecasts", new[] { "TransportTypeId" });
            DropTable("dbo.Forecasts");
        }
    }
}
