﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace GCIForecastApp.Models
{
    [DebuggerDisplay("{ToString()}")]
    public class Organization
    {
        public Organization()
        {
            Contracts = new HashSet<Contract>();
        }

        public int OrganizationId { get; set; }

        [Required, MaxLength(250)]
        [StringLength(250, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public string Name { get; set; }

        public string Site { get; set; }

        public string Description { get; set; }

        public virtual ICollection<Contract> Contracts { get; set; }

        public override string ToString()
        {
            return string.Format("OrganizationId: {0}, Name: {1}, Site: {2}, Description: {3}, Contracts: {4}", OrganizationId, Name, Site, Description, Contracts.Count);
        }
    }
}