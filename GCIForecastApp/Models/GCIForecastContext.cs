﻿using System.Data.Entity;

namespace GCIForecastApp.Models
{
    public class GciForecastContext : DbContext
    {
        public GciForecastContext() :
            base("name=DefaultConnection")
        {
        }

        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Circuit> Circuits { get; set; }
        public DbSet<TransportType> TransportTypes { get; set; }
        public DbSet<Forecast> Forecasts { get; set; }
    }
}