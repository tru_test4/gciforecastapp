﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace GCIForecastApp.Models
{
    [DebuggerDisplay("{ToString()}")]
    public class Circuit
    {
        public Circuit()
        {
            Forecasts = new HashSet<Forecast>();
        }

        public int CircuitId { get; set; }

        [Required]
        public string From { get; set; }

        [Required]
        public string To { get; set; }

        [Required]
        public decimal BwIn { get; set; }

        [Required]
        public decimal BwOut { get; set; }

        public int TransportTypeId { get; set; }
        public virtual TransportType TransportType { get; set; }

        public int ContractId { get; set; }
        public virtual Contract Contract { get; set; }

        public virtual ICollection<Forecast> Forecasts { get; set; }

        public override string ToString()
        {
            return string.Format("CircuitId: {0}, From: {1}, To: {2}, BwIn: {3}, BwOut: {4}, TransportTypeId: {5}, ContractId: {6}", CircuitId, From, To, BwIn, BwOut, TransportTypeId, ContractId);
        }
    }
}