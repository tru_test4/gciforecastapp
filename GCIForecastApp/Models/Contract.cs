﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace GCIForecastApp.Models
{
    [DebuggerDisplay("{ToString()}")]
    public class Contract
    {
        public Contract()
        {
            Circuits = new HashSet<Circuit>();
        }

        public int ContractId { get; set; }

        [Required, MaxLength(250)]
        [StringLength(250, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public string Title { get; set; }
        
        [Required]
        public DateTime SignatureDate { get; set; }

        [Required]
        public DateTime ExpirationDate { get; set; }

        #region Navigation properties

        public int OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }

        public virtual ICollection<Circuit> Circuits { get; set; } 

        #endregion

        public override string ToString()
        {
            return string.Format("ContractId: {0}, SignatureDate: {1:d}, ExpirationDate: {2:d}, OrganizationId: {3}, Circuits: {4}", ContractId, SignatureDate, ExpirationDate, OrganizationId, Circuits.Count);
        }
    }
}