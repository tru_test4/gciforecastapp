﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GCIForecastApp.Models
{
    public class Forecast
    {
        public int ForecastId { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public decimal BwIn { get; set; }

        [Required]
        public decimal BwOut { get; set; }

        public int TransportTypeId { get; set; }
        public virtual TransportType TransportType { get; set; }

        public int CircuitId { get; set; }
        public virtual Circuit Circuit { get; set; }


        public override string ToString()
        {
            return string.Format("ForecastId: {0}, StartDate: {1:d}, BwIn: {2:F2}, BwOut: {3:F2}, TransportTypeId: {4}, CircuitId: {5}", ForecastId, StartDate, BwIn, BwOut, TransportTypeId, CircuitId);
        }
    }
}