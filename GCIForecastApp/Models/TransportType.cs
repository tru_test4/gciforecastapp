﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace GCIForecastApp.Models
{
    [DebuggerDisplay("{ToString()}")]
    public class TransportType
    {
        public int TransportTypeId { get; set; }

        [Required, MaxLength(30)]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string Name { get; set; }

        public override string ToString()
        {
            return string.Format("TransportTypeId: {0}, Name: {1}", TransportTypeId, Name);
        }
    }
}