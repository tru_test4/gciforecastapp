﻿using System.Collections;
using System.Linq;
using System.Web;

namespace GCIForecastApp.Models
{
    public class GCIForecastDataProvider
    {
        const string GCIForecastContextKey = "GCIForecastContext";

        public static GciForecastContext DB
        {
            get
            {
                if (HttpContext.Current.Items[GCIForecastContextKey] == null)
                {
                    HttpContext.Current.Items[GCIForecastContextKey] = new GciForecastContext();
                }

                return (GciForecastContext)HttpContext.Current.Items[GCIForecastContextKey];
            }
        }

        public static IEnumerable GetOrganizations()
        {
            return (from oo in DB.Organizations select oo).ToList();
        }

        public static IEnumerable GetTransportTypes()
        {
            return (from tt in DB.TransportTypes select tt).ToList();
        }

        public static IEnumerable GetContracts()
        {
            return (from cc in DB.Contracts select cc).ToList();
        }



         
    }
}